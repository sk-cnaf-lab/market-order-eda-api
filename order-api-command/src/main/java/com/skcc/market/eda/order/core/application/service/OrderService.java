package com.skcc.market.eda.order.core.application.service;

import com.skcc.market.eda.order.core.domain.entity.Order;
import com.skcc.market.eda.order.infrastructure.persistent.jpa.OrderCommandRepository;
import com.skcc.market.eda.common.order.core.object.command.OrderPlacedCommand;
import com.skcc.market.eda.common.order.core.object.event.OrderCreateEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class OrderService implements IOrderService {

    private final OrderCommandRepository orderCommandRepository;
    private final ModelMapper modelmapper;
    private final CommandGateway commandGateway;

    public void doOrderCreateCommandService(OrderCreateEvent event){

        /**
         *  1. 주문 데이터 생성 (pending 상태로 생성)
         */
        {
            Order targetOrder = modelmapper.map(event, Order.class);
            orderCommandRepository.save(targetOrder);
        }
        log.debug("#EVENT : [OrderCreateEvent] 수신");
        log.debug("#Data : 주문 데이터 생성 (pending 상태로 생성) 완료");

        /**
         * 2. OrderPlacedCommand 송신
         */
        log.debug("#Command : [OrderPlacedCommand] 송신");
        commandGateway.send(OrderPlacedCommand.builder()
                    .orderCreateDate(event.getOrderCreateDate())
                    .orderId(event.getOrderId())
                    .orderPrice(event.getOrderPrice())
                    .productId(event.getProductId())
                    .build());
    }
}

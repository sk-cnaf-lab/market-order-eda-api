package com.skcc.market.eda.order.infrastructure.external_system;

import com.skcc.market.eda.order.core.port_infra.external_system.ExternalSampleSystemPort;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExternalSampleSystem implements ExternalSampleSystemPort{

    @Override
    public boolean doExternalSampleSystemLogic(int isExternalError){
        log.debug("[ExternalSampleSystem Called] doExternalSampleSystemLogic::isExternalError::" + isExternalError);
        // do some interface logic with external system

        boolean result = !(isExternalError == 1);

        if(result) log.debug("[ExternalSampleSystem result] Success");
        else log.debug("[ExternalSampleSystem result] Fail");
        
        return result;
    }

}
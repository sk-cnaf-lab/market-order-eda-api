package com.skcc.market.eda.common.product.core.object.command;

import lombok.*;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter @Setter
@Builder
public class ProductRegistCompletedCommand {
    @TargetAggregateIdentifier
    private String aggregateId;             //aggregate id
    private String custId;
    private Date completedDate;
    private Long productId;                   //주문번호
    private int inventory;                 //상품번호
    private int productPrice;                 //주문가격
}

package com.skcc.market.eda.common.order.core.object.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@AllArgsConstructor
@ToString
@Getter
public class CreateSampleDomainAggregateCommand {

  @TargetAggregateIdentifier
  private String id;
  private String sampleData1;
  private String sampleData2;
  int isExternalError;
}
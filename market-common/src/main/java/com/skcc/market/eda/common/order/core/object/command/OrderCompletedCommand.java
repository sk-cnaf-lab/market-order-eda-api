package com.skcc.market.eda.common.order.core.object.command;

import lombok.*;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@ToString
@Getter @Setter
public class OrderCompletedCommand {
    @TargetAggregateIdentifier
    private String aggregateId;             //aggregate id
    private Long orderId;                   //주문번호
    private String custId;
    private Long productId;                 //상품번호
    private int orderPrice;                 //주문가격
    private Date orderCreateDate;           //주문 생성 시간
    private Date orderCompletedDate;        //주문 완료 시간

    @Builder
    public OrderCompletedCommand(Long orderId, Long productId, int orderPrice, Date orderCreateDate, Date orderCompletedDate){
        this.aggregateId = UUID.randomUUID().toString();
        this.orderId = orderId;
        this.productId = productId;
        this.orderPrice = orderPrice;
        this.orderCreateDate = orderCreateDate;
        this.orderCompletedDate = orderCompletedDate;
    }
}

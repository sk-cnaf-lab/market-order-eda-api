package com.skcc.market.eda.common.timeline.core.object.command;

import lombok.*;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter @Setter
@Builder
public class TimelineCompletedCommand {
    @TargetAggregateIdentifier
    private String aggregateId;             //aggregate id
    private Long timelineId;
    private String custId;                  //사용자 id
    private String timelineContent;         //
    private Date createDate;           //생성 시간
    private Date completedDate;        //완료 시간
}

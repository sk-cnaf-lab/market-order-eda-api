package com.skcc.market.eda.order.core.application.object.command;

import com.skcc.market.eda.order.core.domain.entity.OrderStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.Date;
import java.util.UUID;

@ToString
@Getter
public class OrderCreateCommand {
    @TargetAggregateIdentifier
    private String aggregateId;
    private Long orderId;                   //주문번호
    private Long productId;                 //상품번호
    private int orderPrice;                 //주문가격
    private OrderStatusEnum orderStatus;    //주문상태
    private Date orderCreateDate;           //주문 생성 시간

    @Builder
    public OrderCreateCommand(Long orderId, Long productId, int orderPrice){
        this.aggregateId = UUID.randomUUID().toString();
        this.orderId = orderId;
        this.productId = productId;
        this.orderPrice = orderPrice;
        this.orderStatus = OrderStatusEnum.pending;
        this.orderCreateDate = new Date();

    }

}

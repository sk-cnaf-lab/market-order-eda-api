package com.skcc.market.eda.common.comment.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
@Builder
public class CommentCompletedEvent {
    private String custId;          //사용자 id
    private String fromCustId;      //댓글 작성한 user id
    private String noteId;          //게시글 번호
    private String commentId;       //댓글 번호
    private Date createDate;           //생성 시간
    private Date completedDate;        //완료 시간
}

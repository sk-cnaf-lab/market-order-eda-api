package com.skcc.market.eda.order.infrastructure.persistent.jpa;

import com.skcc.market.eda.order.core.domain.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderCommandRepository extends JpaRepository<Order, Long> {
}

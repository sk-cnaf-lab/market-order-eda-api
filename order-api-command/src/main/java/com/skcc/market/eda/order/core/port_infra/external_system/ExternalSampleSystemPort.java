package com.skcc.market.eda.order.core.port_infra.external_system;

public interface ExternalSampleSystemPort {

    public boolean doExternalSampleSystemLogic(int isExternalError);

}
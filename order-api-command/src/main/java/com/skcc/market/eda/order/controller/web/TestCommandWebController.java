package com.skcc.market.eda.order.controller.web;

import com.skcc.market.eda.order.core.application.object.command.OrderCreateCommand;
import com.skcc.market.eda.order.core.application.object.dto.OrderDTO;

import com.skcc.market.eda.common.comment.core.object.command.CommentCompletedCommand;
import com.skcc.market.eda.common.like.core.object.command.LikeCompletedCommand;
import com.skcc.market.eda.common.order.core.object.command.OrderCompletedCommand;
import com.skcc.market.eda.common.product.core.object.command.ProductRegistCompletedCommand;
import com.skcc.market.eda.common.timeline.core.object.command.TimelineCompletedCommand;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;


@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/order-command")
public class TestCommandWebController {

    private final CommandGateway commandGateway;
    private final ModelMapper modelMapper;


    @PostMapping("/order/create")
    public ResponseEntity<Object> createOrder(@RequestBody OrderDTO orderDTO) throws Exception {
        log.debug("[OrderCommandWebController 호출]: createOrder");

        /**
         * 1. Command 발행
         */

        commandGateway.sendAndWait(OrderCreateCommand.builder()
                .orderId(orderDTO.getOrderId())
                .orderPrice(orderDTO.getOrderPrice())
                .productId(orderDTO.getProductId())
                .build());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/order/complete")
    public ResponseEntity<Object> createOrder(@RequestBody OrderCompletedCommand command) throws Exception {
        log.debug("[OrderCommandWebController 호출]: createOrder");

        /**
         * 1. Command 발행
         */
        OrderCompletedCommand orderCompletedCommand = command;
        orderCompletedCommand.setAggregateId(UUID.randomUUID().toString());
        commandGateway.sendAndWait(orderCompletedCommand);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/like/complete")
    public ResponseEntity<Object> createOrder(@RequestBody LikeCompletedCommand command) throws Exception {
        log.debug("[OrderCommandWebController 호출]: createOrder");

        /**
         * 1. Command 발행
         */
        LikeCompletedCommand likeCompletedCommand = command;
        likeCompletedCommand.setAggregateId(UUID.randomUUID().toString());
        commandGateway.sendAndWait(likeCompletedCommand);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/comment/complete")
    public ResponseEntity<Object> createOrder(@RequestBody CommentCompletedCommand command) throws Exception {
        log.debug("[OrderCommandWebController 호출]: createOrder");

        /**
         * 1. Command 발행
         */
        CommentCompletedCommand commentCompletedCommand = command;
        commentCompletedCommand.setAggregateId(UUID.randomUUID().toString());
        commandGateway.sendAndWait(commentCompletedCommand);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/timeline/complete")
    public ResponseEntity<Object> createOrder(@RequestBody TimelineCompletedCommand command) throws Exception {
        log.debug("[OrderCommandWebController 호출]: createOrder");

        /**
         * 1. Command 발행
         */
        TimelineCompletedCommand timelineCompletedCommand = command;
        timelineCompletedCommand.setAggregateId(UUID.randomUUID().toString());
        commandGateway.sendAndWait(timelineCompletedCommand);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/product/complete")
    public ResponseEntity<Object> createOrder(@RequestBody ProductRegistCompletedCommand command) throws Exception {
        log.debug("[OrderCommandWebController 호출]: createOrder");

        /**
         * 1. Command 발행
         */
        ProductRegistCompletedCommand productRegistCompletedCommand = command;
        productRegistCompletedCommand.setAggregateId(UUID.randomUUID().toString());
        commandGateway.sendAndWait(productRegistCompletedCommand);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
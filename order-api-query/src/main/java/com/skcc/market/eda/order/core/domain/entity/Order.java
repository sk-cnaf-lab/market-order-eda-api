package com.skcc.market.eda.order.core.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Table(name = "MK_ORDER")
@Entity
@Getter
@Setter
//@Builder
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;                   //주문번호
    private Long productId;                 //상품번호
    private int orderPrice;                 //주문가격
    private OrderStatusEnum orderStatus;    //주문상태
    private Date orderReqDate;              //초기 주문 요청 시간
    private Date orderSucceedDate;          //주문 완료 시간
    private Date updateDate;                //마지막 수정 시간

//    @Builder
//    Order(OrderDTO orderDTO){
//        this.orderId = orderDTO.getOrderId();
//        this.productId = orderDTO.getProductId();
//        this.orderPrice = orderDTO.getOrderPrice();
//        this.orderStatus = orderDTO.getOrderStatus();
//        this.orderReqDate = orderDTO.getOrderReqDate();
//        this.updateDate = orderDTO.getUpdateDate();
//    }

}

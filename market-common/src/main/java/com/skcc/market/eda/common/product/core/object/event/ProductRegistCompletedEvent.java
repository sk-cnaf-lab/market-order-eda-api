package com.skcc.market.eda.common.product.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
@Builder
public class ProductRegistCompletedEvent {
    private String custId;
    private Date completedDate;
    private Long productId;                   //주문번호
    private int inventory;                 //상품번호
    private int productPrice;                 //주문가격
}

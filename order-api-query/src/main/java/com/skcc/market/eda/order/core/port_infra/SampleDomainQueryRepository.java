package com.skcc.market.eda.order.core.port_infra;

import java.util.List;
import java.util.Optional;

import com.skcc.market.eda.order.core.application.object.SampleDomainQueryResponseDTO;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SampleDomainQueryRepository extends JpaRepository<SampleDomainQueryResponseDTO, String> {

    Optional<SampleDomainQueryResponseDTO> findByDomainId(String domainId);
    List<SampleDomainQueryResponseDTO> findAllByDomainId(String domainId);
}
package com.skcc.market.eda;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SampleQueryApiApplication {

	private static final String PROPERTIES =
									"spring.config.location="
									+ "classpath:/config/application/";
		
	public static void main(String[] args) {
		//SpringApplication.run(SampleQueryApiApplication.class, args);
		new SpringApplicationBuilder(SampleQueryApiApplication.class)
            .properties(PROPERTIES)
            .run(args);
	}

}

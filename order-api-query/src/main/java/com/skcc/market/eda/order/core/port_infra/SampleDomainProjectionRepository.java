package com.skcc.market.eda.order.core.port_infra;

import com.skcc.market.eda.order.core.application.object.SampleDomainProjctionDTO;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SampleDomainProjectionRepository extends JpaRepository<SampleDomainProjctionDTO, String> {
}
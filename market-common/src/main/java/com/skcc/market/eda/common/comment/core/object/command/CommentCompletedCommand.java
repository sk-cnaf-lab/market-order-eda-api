package com.skcc.market.eda.common.comment.core.object.command;

import lombok.*;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter @Setter
@Builder
public class CommentCompletedCommand {
    @TargetAggregateIdentifier
    private String aggregateId;             //aggregate id
    private String custId;          //사용자 id
    private String fromCustId;      //댓글 작성한 user id
    private String noteId;          //게시글 번호
    private String commentId;       //댓글 번호
    private Date createDate;           //생성 시간
    private Date completedDate;        //완료 시간
}

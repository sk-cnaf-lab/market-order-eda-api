package com.skcc.market.eda.common.like.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
@Builder
public class LikeCompletedEvent {
    private String custId;          //사용자 id
    private String fromCustId;      //좋아요 누른 user id
    private String boardId;          //게시글 또는 댓글 번호
    private Date createDate;           //생성 시간
    private Date completedDate;        //완료 시간
}


package com.skcc.market.eda.common.order.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
@Builder
public class OrderCreatedEvent {
    private Long orderId;                   //주문번호
    private Long productId;                 //상품번호
    private int orderPrice;                 //주문가격
    private Date orderCreateDate;           //주문 생성 시간
}

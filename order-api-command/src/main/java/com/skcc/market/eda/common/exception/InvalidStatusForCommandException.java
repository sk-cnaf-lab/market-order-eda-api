package com.skcc.market.eda.common.exception;

public class InvalidStatusForCommandException extends RuntimeException {

	private static final long serialVersionUID = -5474328473401276803L;

    public InvalidStatusForCommandException(Object cmd) {
        super("The Current Aggregate status is not allowable for Command [" 
            + cmd.toString() 
            + "]");
    }
    
    public InvalidStatusForCommandException(Object cmd, Object status) {
        super("The Current Aggregate status ["+ ((status==null)? "null":status.toString())
            +"] is not allowable for Command [" 
            + cmd.toString() 
            + "]");
	}

}
package com.skcc.market.eda.common.order.core.object.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@ToString
@Getter
public class OrderPlacedCommand {
    @TargetAggregateIdentifier
    private String aggregateId;
    private Long orderId;                   //주문번호
    private Long productId;                 //상품번호
    private int orderPrice;                 //주문가격
    private Date orderCreateDate;           //주문 생성 시간

    @Builder

    public OrderPlacedCommand(Long orderId, Long productId, int orderPrice, Date orderCreateDate){
        this.aggregateId = UUID.randomUUID().toString();
        this.orderId = orderId;
        this.productId = productId;
        this.orderPrice = orderPrice;
        this.orderCreateDate = orderCreateDate;
    }
}

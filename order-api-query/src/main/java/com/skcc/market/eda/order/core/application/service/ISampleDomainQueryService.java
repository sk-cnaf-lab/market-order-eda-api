package com.skcc.market.eda.order.core.application.service;

import java.util.List;

import com.skcc.market.eda.order.core.application.object.SampleDomainProjctionDTO;
import com.skcc.market.eda.order.core.application.object.SampleDomainQueryResponseDTO;
import com.skcc.market.eda.order.core.application.object.SampleDomainRequestDTO;

public interface ISampleDomainQueryService {

    public void putSampleDataAggregateStatus( SampleDomainProjctionDTO sampleDomainProjctionDTO );

    public List<SampleDomainQueryResponseDTO> getDomainQueryData(SampleDomainRequestDTO sampleDomainRequestDTO );
}

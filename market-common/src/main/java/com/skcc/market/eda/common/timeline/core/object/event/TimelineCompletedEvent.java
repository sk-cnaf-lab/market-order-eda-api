package com.skcc.market.eda.common.timeline.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
@Builder
public class TimelineCompletedEvent {
    private Long timelineId;
    private String custId;                  //사용자 id
    private String timelineContent;         //
    private Date createDate;           //생성 시간
    private Date completedDate;        //완료 시간
}

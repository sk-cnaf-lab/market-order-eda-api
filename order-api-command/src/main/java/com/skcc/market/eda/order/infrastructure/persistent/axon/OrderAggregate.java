package com.skcc.market.eda.order.infrastructure.persistent.axon;

import com.skcc.market.eda.order.core.application.object.command.OrderCreateCommand;
import com.skcc.market.eda.order.core.domain.entity.OrderStatusEnum;
import com.skcc.market.eda.common.comment.core.object.command.CommentCompletedCommand;
import com.skcc.market.eda.common.comment.core.object.event.CommentCompletedEvent;
import com.skcc.market.eda.common.like.core.object.command.LikeCompletedCommand;
import com.skcc.market.eda.common.like.core.object.event.LikeCompletedEvent;
import com.skcc.market.eda.common.order.core.object.command.OrderCompletedCommand;
import com.skcc.market.eda.common.order.core.object.command.OrderPlacedCommand;
import com.skcc.market.eda.common.order.core.object.event.OrderCompletedEvent;
import com.skcc.market.eda.common.order.core.object.event.OrderCreateEvent;
import com.skcc.market.eda.common.order.core.object.event.OrderCreatedEvent;
import com.skcc.market.eda.common.product.core.object.command.ProductRegistCompletedCommand;
import com.skcc.market.eda.common.product.core.object.event.ProductRegistCompletedEvent;
import com.skcc.market.eda.common.timeline.core.object.command.TimelineCompletedCommand;
import com.skcc.market.eda.common.timeline.core.object.event.TimelineCompletedEvent;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Aggregate
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class OrderAggregate {
    @AggregateIdentifier
    @Id
    private String aggregateId;
    private Long orderId;
    private Long productId;                 //상품번호
    private int orderPrice;                 //주문가격
    private OrderStatusEnum orderStatus;    //주문상태
    private Date orderCreateDate;           //주문 생성 시간

    @CommandHandler
    public OrderAggregate(OrderCreateCommand command) throws Exception {
        log.debug("#Command : [OrderCreateCommand] 수신");
        this.aggregateId = command.getAggregateId();
        this.orderStatus = command.getOrderStatus();
        this.orderId = command.getOrderId();
        this.orderPrice = command.getOrderPrice();
        this.orderCreateDate = command.getOrderCreateDate();

        AggregateLifecycle.apply(OrderCreateEvent.builder()
                .orderCreateDate(command.getOrderCreateDate())
                .orderId(command.getOrderId())
                .orderPrice(command.getOrderPrice())
                .productId(command.getProductId())
                .build());
        log.debug("#Event : [OrderCreateEvent] 송신");

    }

    @CommandHandler
    public OrderAggregate(OrderPlacedCommand command) throws  Exception{
        log.debug("#Command : [OrderPlacedCommand] 수신");
        this.aggregateId = command.getAggregateId();
        this.orderId = command.getOrderId();
        this.orderCreateDate = command.getOrderCreateDate();
        this.orderPrice = command.getOrderPrice();

        AggregateLifecycle.apply(OrderCreatedEvent.builder()
                .productId(command.getProductId())
                .orderPrice(command.getOrderPrice())
                .orderId(command.getOrderId())
                .orderCreateDate(command.getOrderCreateDate())
                .build());

        log.debug("#Event : [OrderCreatedEvent] 송신");
    }

    @CommandHandler
    public OrderAggregate(OrderCompletedCommand command) {
        this.aggregateId = command.getAggregateId();
        AggregateLifecycle.apply(OrderCompletedEvent.builder()
        .custId(command.getCustId())
        .orderCompletedDate(command.getOrderCompletedDate())
        .orderCreateDate(command.getOrderCreateDate())
        .orderId(command.getOrderId())
        .orderPrice(command.getOrderPrice())
        .productId(command.getProductId())
        .build());
        log.debug("#Event : [OrderCompletedEvent] 송신");
    }

    @CommandHandler
    public OrderAggregate(LikeCompletedCommand command){
        this.aggregateId = command.getAggregateId();
        AggregateLifecycle.apply(LikeCompletedEvent.builder()
        .boardId(command.getBoardId())
        .completedDate(command.getCompletedDate())
        .createDate(command.getCreateDate())
        .custId(command.getCustId())
        .fromCustId(command.getFromCustId())
        .build());
        log.debug("#Event : [LikeCompletedEvent] 송신");
    }

    @CommandHandler
    public OrderAggregate(TimelineCompletedCommand command){
        this.aggregateId = command.getAggregateId();
        AggregateLifecycle.apply(TimelineCompletedEvent.builder()
                .completedDate(command.getCompletedDate())
                .createDate(command.getCreateDate())
                .custId(command.getCustId())
                .build());
        log.debug("#Event : [TimelineCompletedEvent] 송신");
    }

    @CommandHandler
    public OrderAggregate(CommentCompletedCommand command){
        this.aggregateId = command.getAggregateId();
        AggregateLifecycle.apply(CommentCompletedEvent.builder()
        .commentId(command.getCommentId())
        .completedDate(command.getCompletedDate())
        .createDate(command.getCreateDate())
        .custId(command.getCustId())
        .fromCustId(command.getFromCustId())
        .build());
        log.debug("#Event : [CommentCompletedEvent] 송신");
    }

    @CommandHandler
    public OrderAggregate(ProductRegistCompletedCommand command){
        this.aggregateId = command.getAggregateId();
        AggregateLifecycle.apply(ProductRegistCompletedEvent.builder()
        .completedDate(command.getCompletedDate())
        .custId(command.getCustId())
                .inventory(command.getInventory())
                .productId(command.getProductId())
                .productPrice(command.getProductPrice())
                .build());
        log.debug("#Event : [ProductRegistCompletedEvent] 송신");
    }
}
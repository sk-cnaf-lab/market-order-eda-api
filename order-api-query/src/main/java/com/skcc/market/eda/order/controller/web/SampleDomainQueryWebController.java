package com.skcc.market.eda.order.controller.web;

import java.util.List;

import com.skcc.market.eda.common.order.core.object.query.SampleDomainQueryMessage;
import com.skcc.market.eda.order.core.application.object.SampleDomainQueryResponseDTO;
import com.skcc.market.eda.order.core.application.object.SampleDomainRequestDTO;
import com.skcc.market.eda.order.core.application.service.ISampleDomainQueryService;

import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/sample")
public class SampleDomainQueryWebController {

    private final QueryGateway queryGateway;
    private final ISampleDomainQueryService sampleDomainQueryService;

    @GetMapping("/message")
    public ResponseEntity<Object> querySampleDomainByMessage(@RequestBody SampleDomainRequestDTO sampleDomainRequestDTO){
        log.debug("[Web Controller Called] querySampleDomainByMessage");

        SampleDomainQueryMessage sampleDomainQueryMessage = new SampleDomainQueryMessage(
                                                                        sampleDomainRequestDTO.getId()
                                                                        , sampleDomainRequestDTO.getDomainId() );

        List<SampleDomainQueryResponseDTO> queryResult = queryGateway.query(sampleDomainQueryMessage, ResponseTypes.multipleInstancesOf(SampleDomainQueryResponseDTO.class)).join();

        return new ResponseEntity<>( queryResult, HttpStatus.OK);
    }

    @GetMapping("/rest")
    public ResponseEntity<Object> querySampleDomainByDirect(@RequestBody SampleDomainRequestDTO sampleDomainRequestDTO){
        log.debug("[Web Controller Called] querySampleDomainByDirect");

        return new ResponseEntity<>( sampleDomainQueryService.getDomainQueryData( sampleDomainRequestDTO )
                                    , HttpStatus.OK);
    }
}
//package com.skcc.market.eda.order.core.application.service;
//
//import com.skcc.market.eda.common.order.core.object.event.OrderCreateEvent;
//import com.skcc.market.eda.order.core.domain.entity.Order;
//import com.skcc.market.eda.order.infrastructure.persistent.jpa.OrderCommandRepository;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.axonframework.commandhandling.gateway.CommandGateway;
//import org.modelmapper.ModelMapper;
//import org.springframework.stereotype.Service;
//
//@Slf4j
//@RequiredArgsConstructor
//@Service
//public class OrderQueryService implements IOrderQueryService {
//
//    private final OrderCommandRepository orderCommandRepository;
//    private final ModelMapper modelmapper;
//    private final CommandGateway commandGateway;
//
//    public void doOrderCreateCommandService(OrderCreateEvent event){
//
//        /**
//         *  1. 주문 데이터 생성 (pending 상태로 생성)
//         */
//        {
//            Order targetOrder = modelmapper.map(event, Order.class);
//            orderCommandRepository.save(targetOrder);
//        }
//        log.debug("[OrderCreateEvent] 수신");
//        log.debug("주문 데이터 생성 (pending 상태로 생성) 완료");
//    }
//}

//package com.skcc.market.eda.order.controller.handler;
//
//import com.skcc.market.eda.common.order.core.object.event.OrderCreateEvent;
//import com.skcc.market.eda.order.core.application.service.IOrderQueryService;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.axonframework.config.ProcessingGroup;
//import org.axonframework.eventhandling.EventHandler;
//import org.axonframework.eventhandling.Timestamp;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.time.Instant;
//
//@AllArgsConstructor
//@Slf4j
//@Component
//@ProcessingGroup("OrderQueryEventsLocal")
//public class OrderEventHandler {
//    @Autowired
//    IOrderQueryService iOrderCommandService;
//
//    @EventHandler
//    public void on(OrderCreateEvent event, @Timestamp Instant instant) throws Exception {
//        log.debug("[OrderEventHandler Called] OrderCreateEvent");
//        log.debug("projecting {} , timestamp : {}", event, instant.toString());
//
//        iOrderCommandService.doOrderCreateCommandService(event);
//    }
//
//}

package com.skcc.market.eda.order.core.application.object.dto;

import com.skcc.market.eda.order.core.domain.entity.OrderStatusEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class OrderDTO {
    private Long orderId;                   //주문번호
    private Long productId;                 //상품번호
    private int orderPrice;                 //주문가격
    private OrderStatusEnum orderStatus;    //주문상태
    private Date orderReqDate;              //초기 주문 요청 시간
    private Date orderSucceedDate;          //주문 완료 시간
    private Date updateDate;                //마지막 수정 시간


    @Builder
    OrderDTO(Long orderId, Long productId, int orderPrice){
        this.orderId = orderId;
        this.productId = productId;
        this.orderPrice = orderPrice;
        this.orderStatus = OrderStatusEnum.pending;
    }
}
